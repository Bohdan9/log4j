package models;

public interface Reader {
    String read();
}
