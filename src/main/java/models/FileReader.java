package models;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileReader implements Reader {

    final static Logger logger = Logger.getLogger(FileReader.class);


    public String read() {
        String message = "";
        try {
            URI url = ClassLoader.getSystemResource("message.txt").toURI();
            logger.debug("DEBUG MESSAGE");
            logger.info("INFO");
            logger.warn("Warn!");
            logger.error("ERRORR!!!!!");
            logger.fatal("FATAL!");
            byte[] fileBytes = Files.readAllBytes(Paths.get(url));
            logger.info("Bite array from file have been gotten");
            message = new String(fileBytes);
            logger.info("Message from file have been gotten");
        } catch (URISyntaxException | NullPointerException| IOException e) {
            logger.error(e);

        }
        return message;
    }
}
